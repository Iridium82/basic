<?php
//$symBigRu = array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Э','Ю','Я');
//$symSmRu = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','э','ю','я');
//$symBigEn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
//$symSmEn = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
//$symNum = array('0','9','8','7','6','5','4','3','2','1');
//$symPunctuation1 = array('-' , ':' , '/' , '\' , ',', ' ', '.');
//$symPunctuation2 = array('. ','! ','? ');
//$colorWords = array('HTML','PHP','ASP','ASP.NET','Java');

function run($page, &$pageCount)
{

    $data = generateData();
    $itemsPerPage = 18;
    $pageCount = calculatePageCount($data, $itemsPerPage);
    if (checkPageNumber($page, $pageCount)) {
        return readData($data, $page, $itemsPerPage);
    } else {
        return 'Page not found';
    }
}

function generateData()
{
    $text = file_get_contents('data/text_windows1251.txt');
    $text = mb_convert_encoding($text, 'utf-8', 'windows-1251');

    $paragraphsSeparator = "\r\n";
    $paragraphs = explode($paragraphsSeparator,$text);

    if ($paragraphs == false){
        $paragraphs=$text;

    }
    $data = array();


    foreach ($paragraphs as $key => $value) {
        $paragr = '';
        $sentences = splitToSentences($value);
        foreach($sentences as $sent){

            $sent = strongToSent($sent);
            $paragr .= $sent;
        }

        $data[] = colorText(trim($paragr)) . '</br>';
    }
    return $data;
}

function calculatePageCount($data, $itemsPerPage)
{
    return ceil(count($data)/$itemsPerPage);
}

function checkPageNumber($page, $pageCount)
{
    return $page > 0 && $page <= $pageCount;
}

function readData($data, $page, $itemsPerPage)
{
    $offset = ($page - 1) * $itemsPerPage;
    return array_slice($data, $offset, $itemsPerPage);
}


function colorText($text)
{
    $colorWords = array('HTML','PHP','ASP','ASP.NET','Java');
    $symPunctuation1 = array('-' , ':' , '/' , '!' , ',', ' ', '.','?','\r\n','<');
    $colorText = '';
    $countAll = 0;
    //Вычисление общего числа совпадений
    foreach($colorWords as $word){
        $countAll += mb_substr_count($text, $word);
    }
    //Проходим по всем совпадениям
    for ($i=0; $i<=$countAll; $i++){
        $word = '';
        //Вычисляем первое совпадение (позиция, слово)
        $pos = calcPosision($text, $colorWords);
        //var_dump($pos);
        //Вычисляем следующий символ за словом
        $nextSymbol = mb_substr($text,($pos[0]+mb_strlen($pos[1])),1);
        // Проверяем, является следующий символ разделителем между словами ($symPunctuation1)
        foreach ($symPunctuation1 as $symbol){
            $flag=false;
            if ($symbol == $nextSymbol){
                $flag = true;
                break;
            }
        }
        $start = mb_substr($text,0,$pos[0]);
        ($flag == true)?$colorText .= $start.'<span style="color:red">'.$pos[1].'</span>':$colorText .= $start.$pos[1];
        $text = mb_substr($text,($pos[0]+mb_strlen($pos[1])));
    }
    $colorText.=$text;
    return $colorText;
}

function splitToSentences(string $paragraph):array
{
    $symPunctuation2 = array('. ','! ','? ');

    while ($paragraph != ''){
        $pos = calcPosision($paragraph, $symPunctuation2);
        $sentences[] = mb_substr($paragraph, 0, ($pos[0]+2));
        $paragraph = mb_substr($paragraph, ($pos[0]+2));
    }
    return $sentences;
}

function calcPosision($text, $arr):array
{

    $pos1 = mb_strlen($text);
    $word = '';
    for ($i = 0; $i < count($arr); $i++){
        $pos[$i] = mb_stripos($text,$arr[$i]);
        if ($pos[$i]!==false && $pos[$i] <= $pos1) {
            $pos1 = $pos[$i];
            $word = $arr[$i];
        }
    }
    $arrPos[0] = $pos1;
    $arrPos[1] = $word;
    return $arrPos;
}

function splitToWords(string $sentences):array
{
    $symPunctuation2 = array(' ','/',':');

    while ($sentences != ''){
        $pos = calcPosision($sentences, $symPunctuation2);
        $words[] = mb_substr($sentences, 0, ($pos[0]+2));
        $sentences = mb_substr($sentences, ($pos[0]+2));
    }
    return $words;
}

function strongToSent(string $sentence):string
{
    $first = mb_substr($sentence, 0, 1);
    $strongSent = '<b>'.$first.'</b>'.mb_substr($sentence, 1);
    return $strongSent;
}