<?php
class ViewError
{
    private $error;

    public function __construct($error)
    {
        $this->error = $error;
    }

    public function render()
    {
        include 'view/errortemplate.php';
    }
}