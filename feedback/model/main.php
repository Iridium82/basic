<?php
<?php
class Model
{
    private $page;
    private $data;
    private $itemsPerPage = 10;
    private $pageCount;
    private $db;
    private $action;


    public function __construct($page, $action)
    {
        require '/model/DBClass.php';
        $this->$action;

        $this->page = $page;
        $this->db = $DBClass::getInstance();
        $this->generateData();
    }

    public function run()
    {
        $this->calculatePageCount();
        if ($this->checkPageNumber()) {
            $this->data = $this->readData();
        } else {
            $this->data = 'Page not found';
        }
    }

    private function generateData()
    {
        switch ($this->action){
            case "show": showRecord();
            case "insert": insertRecord();
            case "delete":deleteRecord();
            case "edit":editRecord();
            default: showRecord();
        }

    }



    private function calculatePageCount()
    {
        $this->pageCount = ceil(count($this->data)/$this->itemsPerPage);
    }

    private function checkPageNumber()
    {
        return $this->page > 0 && $this->page <= $this->pageCount;
    }

    private function readData()
    {

        $db = new DBClass(DB_HOSTNAME,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
        $query = "SELECT id, user, m_text, m_time FROM message ORDER BY id DESC LIMIT $offset, $itemsPerPpage";
        $offset = ($this->page - 1) * $this->itemsPerPage;
        $result = $db->selectCell($query);

        return $result;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getPageCount()
    {
        return $this->pageCount;
    }

}