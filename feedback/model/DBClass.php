<?php
class DBClass
{
    private $dbName;
    private $dbUser;
    private $dbPsw;
    private $dbHost;
    private $dbTable;
    //параметр для подстановки в запросе
    private $symQuery = "{?}";
    //для контроля за единственностью создания экземпляра
    private static $_instance = null;

    //Контроль за созданием 1 экземпляра (паттерн сингелтон)
    public static function getInstance()
    {
        if (self::$_instance != null) {
            return self::$_instance;
        }

        return new self;
    }

    private function __construct()
    {
        $this -> dbName = DB_DATABASE;
        $this -> dbHost = DB_HOSTNAME;
        $this -> dbPsw = DB_PASSWORD;
        $this -> dbTable = DB_TABLE;
        $this -> dbUser = DB_USERNAME;

        if (!($link = mysqli_connect($this->dbHost, $this->dbUser, $this->dbPsw, $this->dbName)))
            die('Не удалось подключиться к базе данных' . mysqli_connect_error() . '</body></html>');
        //Устанавливаем кодировку символов
        if (!mysqli_set_charset($link, 'utf8'))
            die('Ошибка при установке кодировки: ' . mysqli_error($link) . '</body></html>');

    }

    /*
     * Формирование строки запроса
     * @param общий запрос $query, параметры $param
     * @return итоговый запрос $query
     */
    private function getQuery($query, $param)
    {
        if ($param){
            for ($i = 0; $i < count($param); $i++) {
                $pos = strpos($query, $this->symQuery);
                $arg = "'" . $param . "'";
                $query = str_replace($query, $arg, $pos, str_len($this->symQuery));
            }
        }
        return $query;
    }

    private function resultToArray($result_set)
    {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false){
            $array[] = $row;
        }
        return $array;
    }

    public function selectAll($query, $param=false)
    {
        $result_set = $this->mysqli->query($this->getQuery($query,$param));
        if (!$result_set) return false;
        else return $this->resultToArray($result_set);

    }

    public function selectRow($query, $param=false)
    {
        $result_set = $this->mysqli->query($this->getQuery($query,$param));
        if (!$result_set) return false;
        else return $result_set->fetch_assoc();

    }

    public function selectCell($query, $param=false)
    {
        $result_set = $this->mysqli->query($this->getQuery($query, $param));
        if ((!$result_set) || ($result_set->num_rows != 1)) return false;
        else {
            $arr = array_values($result_set->fetch_assoc());
            return $arr[0];
        }
    }

    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }

}