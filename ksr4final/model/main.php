<?php

function dataArray():array
{
    $symBigRu = array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Э','Ю','Я');
    $symSmRu = array('а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','э','ю','я');
    $symBigEn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $symSmEn = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    $symNum = array('0','9','8','7','6','5','4','3','2','1');
    // Массив разделителей слов
    $symPunctuation1 = array(' ','/',':');
    // Массив разделителей предложений
    $symPunctuation2 = array('. ','! ','? ');
    //Разделители для выделяемых слов
    $symPunctuation3 = array('-' , ':' , '/' , '!' , ',', ' ', '.','?','<', '+');
    // Массив слов, которые необходимо выделить цветом
    $colorWords = array('HTML','PHP','ASP','ASP.NET','Java');
    $dataArray = array('symBigRu' => $symBigRu,
        'symSmRu' => $symSmRu,
        'symBigEn' => $symBigEn,
        'symSmEn' => $symSmEn,
        'symNum' => $symNum,
        'symPunctuation1' => $symPunctuation1,
        'symPunctuation2' => $symPunctuation2,
        'symPunctuation3' => $symPunctuation3,
        'colorWords' => $colorWords);
    return $dataArray;
}

/*
 * Вычисление количества символов. Символы - все, кроме пробелов и тегов
 */
function calcSymbols($text)
{
    $countSym = 0;
    $text = strip_tags(trim($text));
    $arrayText = explode(" ", $text);
    foreach ($arrayText as $value){
        $countSym += mb_strlen($value);
    }
    return $countSym;
}
/*
 * Вывод по $itemsPerPage абзацев на странице
 */
function run($page, &$pageCount)
{
    $itemsPerPage = 20;
    $data = generateData();
    $pageCount = calculatePageCount($data, $itemsPerPage);
    if (checkPageNumber($page, $pageCount)) {
        return readData($data, $page, $itemsPerPage);
    } else {
        return 'Page not found';
    }
}

/*
 * Преобразование входного текста
 */
function generateData()
{
    $text = file_get_contents('data/text_windows1251.txt');
    $text = mb_convert_encoding($text, 'utf-8', 'windows-1251');
    $paragraphsSeparator = "\r\n";
    $paragraphs = explode($paragraphsSeparator,$text);
    if ($paragraphs == false){
        $paragraphs=$text;
    }
    $data = array();
    $countSym = 0;
    foreach ($paragraphs as $key => $value) {
        $paragr = '';
        $sentences = splitToSentences($value);
        $countSym = calcSymbols($value);
        $countWords = 0;
        foreach($sentences as $sent){
            $countWords += count(splitToWords($sent));
            $sent = colorText($sent);
            $sent = strongToSent($sent);
            $paragr .= $sent;
        }
        $countSent = count($sentences);
        $data[] = $paragr . '</br>'."Статистика: количество предложений: $countSent, кол-во слов: $countWords, кол-во символов: $countSym". '</br>';

    }
    return $data;
}

function calculatePageCount($data, $itemsPerPage)
{
    return ceil(count($data)/$itemsPerPage);
}

function checkPageNumber($page, $pageCount)
{
    return $page > 0 && $page <= $pageCount;
}

function readData($data, $page, $itemsPerPage)
{
    $offset = ($page - 1) * $itemsPerPage;
    return array_slice($data, $offset, $itemsPerPage);
}

/*
 * Вычисление первой позиции разделителя из массива. Результат массив из значений номер позиции - элемент массива
 */
function calculatePosision($text, $arr):array
{
    $pos1 = mb_strlen($text);
    $word = '';
    for ($i = 0; $i < count($arr); $i++){
        $pos[$i] = mb_stripos($text,$arr[$i]);
        if ($pos[$i]!==false && $pos[$i] <= $pos1) {
            $pos1 = $pos[$i];
            $word = $arr[$i];
        }
    }
    $arrPos[0] = $pos1;
    $arrPos[1] = $word;
    return $arrPos;
}

/*
 * Разделение текста на предложения. В качестве разделителя '. ','! ','? '.
 * Следующий символ за разделителем - заглавная буква
 */
function splitToSentences(string $paragraph):array
{
    $dataArray = dataArray();
    $arrayResult = array_merge($dataArray['symBigEn'], $dataArray['symBigRu'], $dataArray['symNum']);
    $symPunctuation2 = $dataArray['symPunctuation2'];
    $notSent = '';
    while ($paragraph != ''){
        $pos = calculatePosision($paragraph, $symPunctuation2);
        if ($pos != false) {
            $nextSym = mb_substr($paragraph, ($pos[0] + 2), 1);
            if ($nextSym != false) {
                if (in_array($nextSym, $arrayResult)) {
                    $sentences[] = $notSent . mb_substr($paragraph, 0, ($pos[0] + 2));
                    $paragraph = mb_substr($paragraph, ($pos[0] + 2));
                    $notSent = '';
                } else {
                    $notSent .= mb_substr($paragraph, 0, ($pos[0] + 2));
                    $paragraph = mb_substr($paragraph, ($pos[0] + 2));
                }
            } else {
                $sentences[] = $notSent . mb_substr($paragraph, 0, ($pos[0] + 2));
                $paragraph = mb_substr($paragraph, ($pos[0] + 2));
            }
        }else{
            $sentences[] = $paragraph;
        }
    }
    return $sentences;
}

/*
 * Разделение текста на слова. Разделитель ' ','/',':'
 */
function splitToWords(string $sentences):array
{
    $symPunctuation1 = dataArray()['symPunctuation1'];
    $sentence = strip_tags(trim($sentences));
    $word = array();
    while ($sentence != ''){
        $pos = calculatePosision($sentence, $symPunctuation1);

        if ($pos != false) {
            $word[] = mb_substr($sentence, 0, ($pos[0]+1));
            $sentence = mb_substr($sentence, ($pos[0]+1));
        }else{
            $word[] = mb_substr($sentence, ($pos[0]+1));
            $sentence = '';
        }
    }
    foreach ($word as $value){
        if ($value != "- ") {$words[] = $value;}
    }
    return $words;

}

/*
 * Выделение первой буквы предложения
 */

function strongToSent(string $sentence):string
{
    $first = mb_substr($sentence, 0, 1);
    if ($first == "<"){
        $pos = mb_strpos($sentence, ">");
        $first = mb_substr($sentence, $pos+1,1);
        $strongSent = mb_substr($sentence, 0,($pos+1)).'<b>'.$first.'</b>'.mb_substr($sentence,$pos+2);
    }else{
        $strongSent = '<b>'.$first.'</b>'.mb_substr($sentence, 1);
    }

    return $strongSent;
}

/*
 * Выделение слов $colorWords
 */

function colorText($text)
{
    $symPunctuation3 = dataArray()['symPunctuation3'];
    $colorWords = dataArray()['colorWords'];
    $arrayAll = array_merge(dataArray()['symBigEn'], dataArray()['symBigRu'], dataArray()['symSmEn'], dataArray()['symSmRu']);

    $colorText = '';
    $countAll = 0;

    foreach($colorWords as $word){
        $countAll += mb_substr_count($text, $word);
    }

    for ($i=0; $i<=$countAll; $i++){

        $pos = calculatePosision($text, $colorWords);
        $nextSymbol = mb_substr($text,($pos[0]+mb_strlen($pos[1])),1);
        $beforeSymbol = mb_substr($text,($pos[0]-1),1);
        $start = mb_substr($text,0,$pos[0]);
        if ((in_array($nextSymbol, $symPunctuation3)) && (!in_array($beforeSymbol, $arrayAll))) {
            $colorText .= $start.'<span style="color:red">'.$pos[1].'</span>';

        }else{
            $colorText .= $start.$pos[1];
        }
         $text = mb_substr($text,($pos[0]+mb_strlen($pos[1])));
    }
    $colorText.=$text;
    return $colorText;
}