<?php

function renderPagination($url, $page, $pageCount)
{
    for ($i = 1; $i <= $pageCount; $i++) {
        if ($i == $page) {
            echo $i, ' ';
        } else {
            echo "<a href=\"$url&page=$i\">$i</a> ";
        }
    }
}