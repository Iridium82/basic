<?php
/**
 * Created by PhpStorm.
 * User: Tanya
 * Date: 22.02.2019
 * Time: 20:38
 */

$page = empty($_GET['page'])?1:intval($_GET['page']);

require 'model/' . $name . '.php';

$data = run($page, $pageCount);
if (is_array($data)) {

    require 'view/' . $name . '.php';
} else {

    require 'view/error.php';
}