<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div>
    <?php
    foreach ($data as $item) {
        echo "<p>$item</p>";
    }
    ?>
</div>
<div>
    <?php
    //Подключаем файл для формирования пагинации
    include 'view/pagination.php';
    //имя выполняемого скрипта (путь/index.php) берем из массива
    //S_SERVER Даже при перемещении проекта в другую папку
    //ссылка будет корректной
    $scriptName = $_SERVER['SCRIPT_NAME'];
    //вызываем функцию пагинации
    //она добавит к основному адресу в первом аргументе
    //параметр page=номер_страницы
    renderPagination($scriptName . '?name=main', $page, $pageCount);
    ?>
</div>
</body>
</html>